<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compra".
 *
 * @property integer $idCompra
 * @property string $NombreProveedor
 * @property string $Fecha
 * @property string $Vendedor_Rut
 *
 * @property Vendedor $vendedorRut
 * @property Detallecompra[] $detallecompras
 */
class Compra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fecha'], 'safe'],
            [['Vendedor_Rut'], 'required'],
            [['NombreProveedor'], 'string', 'max' => 45],
            [['Vendedor_Rut'], 'string', 'max' => 20],
            [['Vendedor_Rut'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedor::className(), 'targetAttribute' => ['Vendedor_Rut' => 'Rut']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCompra' => 'Id Compra',
            'NombreProveedor' => 'Nombre Proveedor',
            'Fecha' => 'Fecha',
            'Vendedor_Rut' => 'Vendedor  Rut',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendedorRut()
    {
        return $this->hasOne(Vendedor::className(), ['Rut' => 'Vendedor_Rut']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetallecompras()
    {
        return $this->hasMany(Detallecompra::className(), ['Compra_idCompra' => 'idCompra']);
    }
}

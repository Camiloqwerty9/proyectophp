<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "venta".
 *
 * @property integer $idVenta
 * @property string $Fecha
 * @property string $Vendedor_Rut
 *
 * @property Detalleventa[] $detalleventas
 * @property Vendedor $vendedorRut
 */
class Venta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'venta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fecha'], 'safe'],
            [['Vendedor_Rut'], 'required'],
            [['Vendedor_Rut'], 'string', 'max' => 20],
            [['Vendedor_Rut'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedor::className(), 'targetAttribute' => ['Vendedor_Rut' => 'Rut']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idVenta' => 'Id Venta',
            'Fecha' => 'Fecha',
            'Vendedor_Rut' => 'Vendedor  Rut',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleventas()
    {
        return $this->hasMany(Detalleventa::className(), ['Venta_idVenta' => 'idVenta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendedorRut()
    {
        return $this->hasOne(Vendedor::className(), ['Rut' => 'Vendedor_Rut']);
    }
}
